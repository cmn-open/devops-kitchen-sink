# FROM debian
FROM node:stretch-slim

RUN apt-get update 

# curl
RUN apt-get install -y curl 

# dhall-to-yaml
RUN apt-get install -y bzip2 wget
RUN wget https://github.com/dhall-lang/dhall-haskell/releases/download/1.39.0/dhall-json-1.7.7-x86_64-linux.tar.bz2 
RUN tar --extract --bzip2 --file dhall-*.bz2
RUN cp ./bin/dhall-to-yaml /usr/local/bin

# kubectl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

# newman
RUN npm install -g newman

# kaf cli
RUN curl https://raw.githubusercontent.com/birdayz/kaf/master/godownloader.sh | BINDIR=$HOME/bin bash
RUN install /root/bin/kaf /usr/local/bin/kaf 
